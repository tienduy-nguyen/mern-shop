import { MetaTags } from 'src/app/components/Layouts/MetaTags';

export default function ProfilePage() {
  return (
    <>
      <MetaTags title="Profile" />

      <main>
        <h1>Profile</h1>
      </main>
    </>
  );
}
