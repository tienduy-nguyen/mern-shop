import { MetaTags } from 'src/app/components/Layouts/MetaTags';

export default function LoginPage() {
  return (
    <>
      <MetaTags title="Login" />

      <main>
        <h1>Login</h1>
      </main>
    </>
  );
}
