import { MetaTags } from 'src/app/components/Layouts/MetaTags';

export default function RegisterPage() {
  return (
    <>
      <MetaTags title="Register" />

      <main>
        <h1>Register</h1>
      </main>
    </>
  );
}
