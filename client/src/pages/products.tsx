import { MetaTags } from 'src/app/components/Layouts/MetaTags';

export default function ProductsPage() {
  return (
    <>
      <MetaTags title="Products" />

      <main>
        <h1>Products</h1>
      </main>
    </>
  );
}
