# E-commerce Project

E-commerce project using **GraphQL** with stack: NestJS - ReactJS(NextJS) - MongoDB


## Features
- Auth: Jwt, refresh-token, verification email
- Send email with Nodemailer+Sendgrid
- Full featured shopping cart
- Product reviews and ratings
- Top products carousel
- Product pagination
- Product search feature
- User profile with orders
- Admin product management
- Admin user management
- Admin Order details page
- Mark orders as delivered option
- Checkout process (shipping, payment method, etc)
- PayPal / credit card integration
- Upload images with Multer + Firebase storage
- Testing: Unit tests, end to end tests
- CI-CD: Github actions

## Technologies

- Backend
  - NodeJS (via NestJS)
  - GraphQL (via @nestjs/graphql)
- Frontend
  - ReactJS (via NextJS)
  - Apollo GraphQL
- Database
  - MongoDB

All features inspired by [ProShop of Awesome Brad Traversy](https://github.com/bradtraversy/proshop_mern)
